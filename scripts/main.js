var worldWidth = 4*1280;
var worldHeight = 4*720;
var floorColor = '#007f00';
var antColor = "#CF0000"
var ants = [];
var startAnts = 1;
var antRadius = 15;
var walkingSpeed = 5;
var foodPheromoneTimer = 50;
var homePheromoneTimer = 25;
var maxPheromoneDistance = 1000;
var smellDistance = 250;
var twopi = 2 * Math.PI;
var foodList = [];
var pheromoneFood = [];
var pheromoneNoFood = [];
var turn = 0;
var anthill = {y:worldHeight/2,x:worldWidth/2,radius:20};
var pheromoneHome = [anthill];
//timeout
var timeoutHome = 1500;
var timeoutFood = 1000;
var lifeForce = 1024*4;
var foodlessArea = 300;

function onLoad(){
    // Define Canvas
    canvas          = document.getElementById("GUICanvas");
    canvas.width    = worldWidth;
    canvas.height   = worldHeight;
    ctx             = canvas.getContext("2d");
    ctx.font = "30px Arial";
    // Generate Game
    for (i=0; i<Number(startAnts); i++){ants[i] = generateAnt();}
    // Manually place food
    canvas.addEventListener("mousedown", function(event){
        var rect = canvas.getBoundingClientRect();
        var ratiox = rect.width/worldWidth;
        var ratioy = rect.height/worldHeight;
        var x = event.x/ratiox;
        var y = event.y/ratioy;
        xdist = x - anthill.x;
        ydist = y - anthill.y;
        absDist = Math.sqrt((xdist*xdist) + (ydist*ydist));
        if (absDist>foodlessArea){  
            foodList.push({x:x,y:y,size:50});}
        }, false);
    // Start Game
    window.requestAnimationFrame(gameLoop);
}

function absoluteDistance(point1,point2){
    xdist = point1.x - point2.x;
    ydist = point1.y - point2.y;
    return Math.sqrt((xdist*xdist) + (ydist*ydist));
}
function generateAnt(){
    localAnt = {x:worldWidth/2,
        y:worldHeight/2,
        radius:antRadius,
        fillStyle:antColor,
        smellDistance:smellDistance,
        walkingSpeed:walkingSpeed,
        targetx:this.x,
        targety:this.y,
        walkDirection:Math.random()*2*Math.PI,
        smellDistance:smellDistance,
        nearestFoodDistance:smellDistance+1,
        //foodList:[],
        //pheromoneHome:[],
        pheromoneTarget:{x:0,y:0,distance:909999999},
        seeFoodPheromone:false,
        seeHomePheromone:false,
        lastFood:false,
        noFood:false,
        //pheromoneFood:[],
        foundFood:false,
        stepsTaken:0,
        tooClose:100,
        lifeForce:lifeForce,
        antColor:antColor,
        hasFood:false
        //seeingList:[]
    }
return localAnt;}

function buttonGenerateAnt(){
    ants[ants.length] = generateAnt();
}

function gameLoop(){
    if (document.getElementById("pause").checked){}
    else{turn = turn+1;
    if (turn%homePheromoneTimer == 0){
        placeHomePheromone();
    }
    if (turn%foodPheromoneTimer == 0){
        placeFoodPheromone();
    }
    drawFloor();
    drawFood();
    drawCircle({x:anthill.x,y:anthill.y}, foodlessArea, "#7d7d7d");
    drawCircle({x:anthill.x,y:anthill.y}, anthill.radius, "#000000");
    drawPheromone();
    if (document.getElementById("drawAnt").checked){drawAnt();}
    calculateNextTurn();
    ctx.fillStyle = "black";
    ctx.fillText(("number of ants: " + ants.length)  , 10, 50);}
    window.requestAnimationFrame(gameLoop);
}

function placeHomePheromone(){
    for (i=0;i<ants.length;i++){
        if(!(ants[i].hasFood)){
            ants[i].seeHomePheromone=false;
            for (j=0;(j<pheromoneHome.length && !(ants[i].seeHomePheromone));j++){
                if ((absoluteDistance(pheromoneHome[j],ants[i])<ants[i].smellDistance/2)){
                    if(j==0){ants[i].stepsTaken=1;}
                    if(pheromoneHome[j].distance>ants[i].stepsTaken){pheromoneHome[j].distance=ants[i].stepsTaken;}
                    ants[i].seeHomePheromone=true;
                }
            }
            if(!(ants[i].seeHomePheromone) && (ants[i].stepsTaken<maxPheromoneDistance)){
                pheromoneHome[pheromoneHome.length] = {x:ants[i].x, y:ants[i].y, timeout:timeoutHome, distance:ants[i].stepsTaken};
            }
        }
    }
}

function placeFoodPheromone(){
    for (i=0;i<ants.length;i++){
        if(ants[i].hasFood){
            ants[i].seeFoodPheromone=false;
            for (j=0;(j<pheromoneFood.length && !(ants[i].seeFoodPheromone));j++){
                if ((absoluteDistance(pheromoneFood[j],ants[i])<ants[i].smellDistance/2)){
                    if(pheromoneFood[j].distance>ants[i].stepsTaken){pheromoneFood[j].distance=ants[i].stepsTaken;}
                    ants[i].seeFoodPheromone=true;
                }
            }
            if(!(ants[i].seeFoodPheromone) && (ants[i].stepsTaken<maxPheromoneDistance)){
                pheromoneFood[pheromoneFood.length] = {x:ants[i].x, y:ants[i].y, timeout:timeoutHome, distance:ants[i].stepsTaken};
            }
        }
    }
}

function drawPheromone(){
    if (document.getElementById("foodPheromones").checked){
        for (i=0;i<pheromoneFood.length;i++){
            drawCircle({x:pheromoneFood[i].x, y:pheromoneFood[i].y}, 5, "#FFFF00");
            if (document.getElementById("numberAnts").checked){
                ctx.fillStyle = "black";
                ctx.fillText((String(pheromoneFood[i].distance) + " " + String(pheromoneFood[i].timeout)), pheromoneFood[i].x, pheromoneFood[i].y);
            }
        }
    }
    for (i=0;i<pheromoneNoFood.length;i++){
        drawCircle({x:pheromoneNoFood[i].x, y:pheromoneNoFood[i].y}, 5, "#FF0000"); 
        if (document.getElementById("numberAnts").checked){
            ctx.fillStyle = "black";
            ctx.fillText((i), pheromoneNoFood[i].x, pheromoneNoFood[i].y);
        }
    }
    if (document.getElementById("homePheromones").checked){
        for (i=0;i<pheromoneHome.length;i++){
            drawCircle({x:pheromoneHome[i].x, y:pheromoneHome[i].y}, 5, "#0000FF");
            if (document.getElementById("numberAnts").checked){
                ctx.fillStyle = "black";
                ctx.fillText((pheromoneHome[i].distance), pheromoneHome[i].x, pheromoneHome[i].y);
            }
        }
    }
}


function drawFloor(){
    ctx.fillStyle =  floorColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawFood(){
    for (i=0;i<foodList.length;i++){
        drawCircle({x:foodList[i].x, y:foodList[i].y}, foodList[i].size, "#00ff00");
    }
}

function drawAnt(){
    for (i=0;i<ants.length;i++){
        a = (Math.PI*2 + ants[i].walkDirection) % (Math.PI*2);
        if(ants[i].hasFood){
            drawCircle({
                x:ants[i].x + Math.cos(ants[i].walkDirection)*ants[i].radius,
                y:ants[i].y + Math.sin(ants[i].walkDirection)*ants[i].radius},
                5,
                "#00ff00");
            //drawCircle({x:ants[i].x, y:ants[i].y}, ants[i].radius, "#000000");
        }
        //drawCircle({x:ants[i].x, y:ants[i].y}, ants[i].radius, "#000000");
        
        r = (Math.floor(ants[i].lifeForce/16)).toString(16);
        if (r.length<2){r = "0" + r;}
        g = (Math.floor(128-(Math.floor(ants[i].lifeForce/16)/2))).toString(16);
        
        if (g.length<2){g = "0" + g;}
        drawCircle({x:ants[i].x, y:ants[i].y}, ants[i].radius-5, "#" + r + g + "00");
        if (document.getElementById("numberAnts").checked){
        ctx.fillStyle = "black";
        ctx.fillText((i), ants[i].x, ants[i].y);}
        if (document.getElementById("smellingDistance").checked){
        ctx.beginPath();
        ctx.arc(ants[i].x, ants[i].y, ants[i].smellDistance, 0, twopi, true);
        ctx.stroke();
        ctx.strokeStyle = "#000000";
        ctx.lineWidth = 1;}
        
    }
}

function drawCircle(a, radius, color){
    ctx.beginPath();
    ctx.arc(a.x, a.y, radius, 0, twopi, true);
    ctx.fillStyle = color;
    ctx.fill();
}

function pheromoneCalculation(pheromone){
    for (i=0;i<pheromone.length;i++){
        pheromone[i].timeout--;
        if(pheromone[i].timeout<0){
            pheromone.splice(i,1);
        }
    }
    return pheromone;
}

function calculateNextTurn(){
    pheromoneHome = pheromoneCalculation(pheromoneHome);
    pheromoneFood = pheromoneCalculation(pheromoneFood);
    pheromoneNoFood = pheromoneCalculation(pheromoneNoFood);
        
    for (i=0;i<ants.length;i++){
        ants[i].lifeForce = ants[i].lifeForce-1;
        if(ants[i].lifeForce<0){ants.splice(i,1);continue;}
        ants[i].foundFood = false;
        if(ants[i].hasFood){
            if(absoluteDistance(ants[i],anthill)<ants[i].smellDistance){
                ants[i].walkDirection = Math.atan2(anthill.y - ants[i].y, anthill.x - ants[i].x);
                if(absoluteDistance(ants[i],anthill)<(ants[i].radius+anthill.radius)){
                    ants[i].hasFood=false;
                    ants[i].lifeForce=lifeForce;
                    ants[i].stepsTaken=0;
                    ants[i].lastFood=false;
                    ants[i].walkingSpeed=walkingSpeed;
                    if (document.getElementById("genAnts").checked){ants[ants.length] = generateAnt();}
                    continue;
                }continue;
            }
            ants[i].pheromoneTarget = {x:ants[i].x,y:ants[i].y,distance:99999999999};
            for (j=0; j<pheromoneHome.length; j++){
                setPheromoneTarget(ants[i],pheromoneHome[j]);
            }
            if(absoluteDistance(ants[i].pheromoneTarget,ants[i])<ants[i].tooClose){
                ants[i].walkDirection = ants[i].walkDirection + Math.random()-0.5;
            }else{
                ants[i].walkDirection = Math.atan2(ants[i].pheromoneTarget.y - ants[i].y, ants[i].pheromoneTarget.x - ants[i].x);
            }
        }
        else{
            ants[i].nearestFoodDistance=ants[i].smellDistance+1;
            for (j=0; j<foodList.length; j++){
                absDist = absoluteDistance(ants[i],foodList[j]);
                if (absDist < ants[i].smellDistance){
                    if(absDist < ants[i].nearestFoodDistance){
                        if(absDist < (ants[i].radius+foodList[j].size)){
                            foodList[j].size--;
                            if(foodList[j].size<0){
                                foodList.splice(j, 1);
                                pheromoneNoFood[pheromoneNoFood.length] = {x:ants[i].x, y:ants[i].y, timeout:timeoutFood};
                                ants[i].lastFood=true;
                                //ants[i].walkingSpeed=3;
                            }
                            ants[i].hasFood=true;
                            ants[i].foundFood = false;
                            ants[i].stepsTaken=0;
                            break;
                        }
                        ants[i].nearestFoodDistance = absoluteDistance(ants[i],foodList[j]);
                        if (document.getElementById("foodLine").checked){
                            drawLine({x:ants[i].x, y:ants[i].y}, {x:foodList[j].x, y:foodList[j].y}, "#0000ff");
                        }
                        ants[i].walkDirection = Math.atan2(foodList[j].y - ants[i].y, foodList[j].x - ants[i].x);
                        ants[i].foundFood = true;
                    }
                }
            }
            if(ants[i].foundFood){continue;}
            else{
                ants[i].pheromoneTarget = {x:ants[i].x,y:ants[i].y,distance:999999999999,timeout:0};
                ants[i].noFood = false;
                for (j=0; j<pheromoneNoFood.length; j++){
                    absDist = absoluteDistance(ants[i],pheromoneNoFood[j]);
                    if (absDist < ants[i].smellDistance){
                        ants[i].noFood = true;
                        if (document.getElementById("foodLine").checked){
                            drawLine({x:ants[i].x, y:ants[i].y}, {x:pheromoneNoFood[j].x, y:pheromoneNoFood[j].y}, "#ff0000");
                        }
                    }
                }
                for (j=0; j<pheromoneFood.length; j++){
                    setPheromoneTarget(ants[i],pheromoneFood[j],j);
                }
                if(absoluteDistance(ants[i].pheromoneTarget,ants[i])<ants[i].tooClose || ants[i].noFood){
                    ants[i].walkDirection = ants[i].walkDirection + Math.random()-0.5;
                    if(pheromoneFood.length<0){
                    pheromoneFood[ants[i].pheromoneTarget.pheromoneNumber].timeout--;}
                }else{
                    ants[i].walkDirection = Math.atan2(ants[i].pheromoneTarget.y - ants[i].y, ants[i].pheromoneTarget.x - ants[i].x);
                    pheromoneFood[ants[i].pheromoneTarget.pheromoneNumber].timeout--;
                }
            }
        }
        
        
    }
    moveAnts(ants);
}

function setPheromoneTarget(ant,pheromone,i){
    absDist = absoluteDistance(ant,pheromone);
    if (absDist < ant.smellDistance){
        if (document.getElementById("foodLine").checked){
            drawLine({x:ant.x, y:ant.y}, {x:pheromone.x, y:pheromone.y}, "#ff0000");
        }
        if (Math.abs(pheromone.distance)<ant.pheromoneTarget.distance){
            ant.pheromoneTarget = {x:pheromone.x, y:pheromone.y, distance:Math.abs(pheromone.distance), pheromoneNumber:i};
        }
    }
}
function moveAnt(ant){
    ant.x = ant.x + Math.cos(ant.walkDirection)*ant.walkingSpeed;
    ant.y = ant.y + Math.sin(ant.walkDirection)*ant.walkingSpeed;
    ant.targetx = ant.x + Math.cos(ant.walkDirection)*ant.walkingSpeed*20;
    ant.targety = ant.y + Math.sin(ant.walkDirection)*ant.walkingSpeed*20;
}

function moveAnts(ants){
    for (i=0;i<ants.length;i++){
        ants[i].stepsTaken++;
        if (ants[i].x<0 || ants[i].x>worldWidth){
            ants[i].x = Math.abs(ants[i].x-worldWidth);
        }
        if (ants[i].y<0 || ants[i].y>worldWidth){
            ants[i].y = Math.abs(ants[i].y-worldHeight);
        }
        ants[i].x = ants[i].x + Math.cos(ants[i].walkDirection)*ants[i].walkingSpeed;
        ants[i].y = ants[i].y + Math.sin(ants[i].walkDirection)*ants[i].walkingSpeed;
        ants[i].targetx = ants[i].x + Math.cos(ants[i].walkDirection)*ants[i].walkingSpeed*20;
        ants[i].targety = ants[i].y + Math.sin(ants[i].walkDirection)*ants[i].walkingSpeed*20;
    }
}

function drawLine(a, b, color){
    ctx.beginPath();
    ctx.moveTo(a.x, a.y);
    ctx.lineTo(b.x, b.y);
    ctx.strokeStyle = color;
    ctx.stroke();
}

